﻿using NLog;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Data.SqlClient;

namespace NlogToDB
{
    public class LoggerService
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        static LoggerService()
        {
        }

        public static void LogException(ILogger logger, Exception ex)
        {
            logger.Info(ex);
        }
        public static void Info(string info)
        {
            _logger.Info(info);
        }

    }
}
