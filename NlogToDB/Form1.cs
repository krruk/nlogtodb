﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using NLog;

namespace NlogToDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                throw new Exception("My custom exception", new Exception("My inner exception lvl 1", new Exception("My inner exception lvl 2")));
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogException(ex);

                Stack<Exception> exceptions = new Stack<Exception>();
                ExceptionHandler.TraverseInnerExceptions(ex, exceptions);
                Exception exception;
                while (exceptions.Count > 0)
                {
                    exception = exceptions.Pop();
                    textBox1.Text += ExceptionHandler.ExceptionInfo(exception) + Environment.NewLine;
                }
            }
        }

    }
}
