﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace NlogToDB
{
    public class ExceptionHandler
    {
        #region Logging Exceptions
        //public static void LogException(ILogger logger, Exception ex)
        //{
        //    LoggerService.LogException(logger, ex);
        //}             
        public static void LogException(Exception ex)
        {
            Stack<Exception> exceptions = new Stack<Exception>();
            TraverseInnerExceptions(ex, exceptions);
            Exception exception;
            while (exceptions.Count > 0)
            {
                exception = exceptions.Pop();
                LoggerService.Info(ExceptionInfo(exception));
            }
            
        }
        #endregion

        #region Display Exceptions 
        public static void DisplayException(Exception ex, string caption)
        {
            //Stack<Exception> exceptions = new Stack<Exception>();
            //TraverseInnerExceptions(ex, exceptions);
            //string message = BuildDisplayMessage(exceptions).ToString();
            string message = ex.Message;
            XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static string ExceptionInfo(Exception ex)
        {
            if (ex.TargetSite != null)
            {
                var method = ex.TargetSite.ReflectedType.FullName + "." + ex.TargetSite.Name;
                var line = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                return method + " Line: " + line.ToString() + " Exception message: " + ex.Message;
            }
            else
            {
                return ex.Message;
            }
        }
        private static StringBuilder BuildDisplayMessage(Stack<Exception> exceptions)
        {
            Exception exception;
            StringBuilder stringBuilder = new StringBuilder();
            while (exceptions.Count > 0)
            {
                exception = exceptions.Pop();
                stringBuilder.AppendLine(exception.Message);
            }
            return stringBuilder;
        }
        #endregion

        //public static void LogAndDisplayException(ILogger logger, Exception ex)
        //{
        //    LoggerService.LogException(logger, ex);
        //    DisplayException(ex);
        //}
        //public static void LogAndDisplayException(Exception ex, string info)
        //{
        //    LoggerService.Info(info);
        //    DisplayException(ex);
        //}
        public static void LogAndDisplayException(Exception ex)
        {
            LogException(ex);
            DisplayException(ex, "Error");
        }
        public static void LogAndDisplayException(Exception ex, string caption)
        {
            LogException(ex);
            DisplayException(ex, caption);
        }
        public static void TraverseInnerExceptions(Exception ex, Stack<Exception> exceptions)
        {
            if (ex is AggregateException)
            {
                foreach (Exception iex in ((AggregateException)ex).InnerExceptions)
                {
                    TraverseInnerExceptions(iex, exceptions);
                }
            }
            else
            {
                if (ex.InnerException != null)
                {
                    TraverseInnerExceptions(ex.InnerException, exceptions);
                }
                exceptions.Push(ex);
            }
        }
    }
}
